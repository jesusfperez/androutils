package app.jesusfperez.com.androutils;

import android.support.v4.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Chronometer;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;
import android.os.Vibrator;

public class OneFragment extends Fragment {

    Chronometer chronometer;
    Button btnStart, btnStop;
    String state = "inactive";
    Long auxChronometer;
    Spinner spinnermin, spinnersec;
    String minutes, seconds;
    CheckBox checkboxlimit, checkboxvibrate, checkboxtoast;


    public OneFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_one, container, false);
        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        btnStart = (Button) getView().findViewById(R.id.btnStart);
        btnStop = (Button) getActivity().findViewById(R.id.btnReset);
        chronometer = (Chronometer) getActivity().findViewById(R.id.Chrono);

        checkboxlimit = (CheckBox) getView().findViewById(R.id.cb1);
        checkboxvibrate = (CheckBox) getView().findViewById(R.id.cb2);
        checkboxtoast = (CheckBox) getView().findViewById(R.id.cb3);

        chronometer.setText("00:00");

        chronometer.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {

            public void onChronometerTick(Chronometer chronometer) {

                /* Chronometer format */
                long time = SystemClock.elapsedRealtime() - chronometer.getBase();
                int h = (int) (time / 3600000);
                int m = (int) (time - h * 3600000) / 60000;
                int s = (int) (time - h * 3600000 - m * 60000) / 1000;
                String mm = m < 10 ? "0" + m : m + "";
                String ss = s < 10 ? "0" + s : s + "";
                chronometer.setText(mm + ":" + ss);

                if (checkboxlimit.isChecked() && chronometer.getText().toString().equalsIgnoreCase(getMinutes() + ":" + getSeconds())) { //When reaches X minutes and seconds.
                    //Define here what happens when the Chronometer reaches the time above.
                    if (checkboxvibrate.isChecked()) {
                        Vibrator v = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
                        v.vibrate(300);
                    }
                    chronometer.stop();
                    if (checkboxtoast.isChecked())
                        Toast.makeText(getContext(), getString(R.string.toastText), Toast.LENGTH_SHORT).show();

                    state = "inactive";
                    checkboxvibrate.setChecked(false);
                    checkboxtoast.setChecked(false);
                    checkboxlimit.setChecked(false);
                    btnStart.setText(getString(R.string.btnStart));
                }
            }
        });

        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (state == "inactive") {
                    //chronometer.setBase(SystemClock.elapsedRealtime() - 10*1000);
                    chronometer.setBase(SystemClock.elapsedRealtime());
                    chronometer.start();
                    state = "active";
                    btnStart.setText(getString(R.string.btnPause));
                    return;
                }

                if (state == "active") {
                    auxChronometer = SystemClock.elapsedRealtime();

                    chronometer.stop();
                    state = "pause";
                    btnStart.setText(getString(R.string.btnContinue));
                    return;
                } else {
                    chronometer.setBase(chronometer.getBase() + SystemClock.elapsedRealtime() - auxChronometer);
                    chronometer.start();
                    state = "active";
                    btnStart.setText(getString(R.string.btnPause));
                }

            }
        });

        btnStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkboxlimit.setChecked(false);
                checkboxvibrate.setChecked(false);
                checkboxtoast.setChecked(false);
                chronometer.stop();
                state = "inactive";
                chronometer.setText("00:00");
                btnStart.setText(getString(R.string.btnStart));

            }
        });

        // Minutes and seconds spinners disabled when app starts

        spinnermin = (Spinner) getView().findViewById(R.id.spinnerMin);
        spinnermin.setEnabled(false);
        spinnersec = (Spinner) getView().findViewById(R.id.spinnerSec);
        spinnersec.setEnabled(false);

        Integer[] array1 = new Integer[60];
        Integer[] array2 = new Integer[59];

        for (int i = 0; i < array1.length; i++) array1[i] = i;

        for (int i = 0; i < array2.length; i++) array2[i] = i + 1;

        // Create an ArrayAdapter using the array and a default spinner layout
        ArrayAdapter<Integer> adapter1 = new ArrayAdapter<Integer>(getContext(), android.R.layout.simple_spinner_item, array1);

        ArrayAdapter<Integer> adapter2 = new ArrayAdapter<Integer>(getContext(), android.R.layout.simple_spinner_item, array2);

        // Specify the layout to use when the list of choices appears
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Apply the adapter to the spinner
        spinnermin.setAdapter(adapter1);
        spinnermin.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        String mins = parent.getSelectedItem().toString();
                        setMinutes(mins);
                        //Toast.makeText(getContext(), elemento, Toast.LENGTH_SHORT).show();
                    }

                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                });
        spinnersec.setAdapter(adapter2);
        spinnersec.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        String secs = parent.getSelectedItem().toString();
                        setSeconds(secs);

                    }

                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                });

        //checkboxlimit
        checkboxlimit = (CheckBox) getView().findViewById(R.id.cb1);

        checkboxlimit.setOnCheckedChangeListener(new CheckBox.OnCheckedChangeListener() {

            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {

                if (buttonView.isChecked()) { // Enable spinners and the others cb when checkboxlimit is checked
                    spinnermin.setEnabled(true);
                    spinnersec.setEnabled(true);


                } else { // disabled spinners and the others cbwhen checkboxlimit is not checked
                    spinnermin.setEnabled(false);
                    spinnersec.setEnabled(false);

                    checkboxtoast.setChecked(false);
                    checkboxvibrate.setChecked(false);
                }

            }
        });


    }

    public String getMinutes() {
        return minutes;
    }

    public void setMinutes(String mins) {

        if (Integer.parseInt(mins) < 10) { // put a '0' when minutes are 0-9
            mins = "0" + mins;
        }

        this.minutes = mins;
    }

    public String getSeconds() {
        return seconds;
    }

    public void setSeconds(String secs) {

        if (Integer.parseInt(secs) < 10) { // put a '0' when seconds are 0-9
            secs = "0" + secs;
        }
        this.seconds = secs;
    }


}
